function pad2D(a::Array{T,2}) where {T<:Any}
    b = zeros(typeof(a[1]), size(a) .+ 2)
    b[2:end-1, 2:end-1], b[2:end-1, 1], b[2:end-1, end], b[1, 2:end-1], b[end, 2:end-1] =
        a, a[:, 1], a[:, end], a[1, :], a[end, :]
    b[1, 1], b[1, end], b[end, 1], b[end, end] = a[1, 1], a[1, end], a[end, 1], a[end, end]
    b
end

function nsewp(a::Array{T,2}) where {T<:Any}
    a[2:end-1, 3:end][:],
    a[2:end-1, 1:end-2][:],
    a[3:end, 2:end-1][:],
    a[1:end-2, 2:end-1][:],
    a[2:end-1, 2:end-1][:]
end

function nsew_eq(a::Array{T,2}) where {T<:Any}
    a[1:end-1, 2:end][:], a[1:end-1, 1:end-1][:], a[2:end, 1:end-1][:], a[2:end, 2:end][:]
end

function nsew(a::Array{T,2}) where {T<:Any}
    a[1:end, 2:end][:], a[1:end, 1:end-1][:], a[2:end, 1:end][:], a[1:end-1, 1:end][:]
end

function adenom(
    pq::Array{T,1},
    ep::Array{T,1},
    een::Array{T,1},
    en::Array{T,1},
    ews::Array{T,1},
    ws::Array{T,1},
) where {T<:Any}
    (
        (pq .* (ep - een) + 2 .* en .* een) .*
        (pq .^ 2 .* (ep - ews) + 4 .* ws .^ 2 .* ews) +
        (pq .* (ep - ews) + 2 .* ws .* ews) .*
        (pq .^ 2 .* (ep - een) + 4 .* en .^ 2 .* een)
    )
end
