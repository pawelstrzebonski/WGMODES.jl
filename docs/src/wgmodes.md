# wgmodes

The `wgmodes` function is a port of the `wgmodes.m` file from
[WGMODES](https://photonics.umd.edu/software/wgmodes/).
It is a vectorial finite difference modesolver. Please consult
the function documentation for a list of arguments.

## Functions

```@docs
WGMODES.wgmodes
```
