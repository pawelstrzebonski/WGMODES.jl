@testset "svmodes function test" begin
    lambda = 1.0
    guess = 3.0
    nmodes = 5
    dx = 1.0
    dy = 1.0
    epsilon = ones(10, 10)
    epsilon[4:7, 4:7] *= 3

    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "0000", "scalar")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "0000", "ex")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "0000", "ey")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "AAAA", "scalar")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "AAAA", "ex")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "AAAA", "ey")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "SSSS", "scalar")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "SSSS", "ex")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "SSSS", "ey")

    GC.gc()

    dx = ones(10)
    dy = ones(10)
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "0000", "scalar")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "0000", "ex")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "0000", "ey")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "AAAA", "scalar")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "AAAA", "ex")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "AAAA", "ey")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "SSSS", "scalar")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "SSSS", "ex")
    @test_nowarn phi, neff =
        WGMODES.svmodes(lambda, guess, nmodes, dx, dy, epsilon, "SSSS", "ey")

    GC.gc()

    @test_throws Exception WGMODES.svmodes(
        lambda,
        guess,
        nmodes,
        dx,
        dy,
        epsilon,
        "SSSS",
        "XXX",
    )
    @test_throws Exception WGMODES.svmodes(
        lambda,
        guess,
        nmodes,
        dx,
        dy,
        epsilon,
        "SSSSS",
        "XXX",
    )
    @test_throws Exception WGMODES.svmodes(
        lambda,
        guess,
        nmodes,
        dx,
        dy,
        epsilon,
        "XSSS",
        "XXX",
    )

    GC.gc()
end

@testset "svmodes result test" begin
    # Simple test comparing to values obtained from original WGMODES code as
    # run on Octave
    lambda = 1.0
    dx = dy = 0.01
    n = ones(200, 300)
    n[50:100, 100:200] *= 3
    eps = n .^ 2
    guess = 3.0
    nmodes = 6
    boundary = "0000"
    field = "scalar"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.8568, 2.7515, 2.5680, 2.5069, 2.3872, 2.2909])
    end

    GC.gc()

    lambda = 1.0
    dx = dy = 0.01
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 3.0
    nmodes = 6
    boundary = "0000"
    field = "scalar"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.8594, 2.7567, 2.5801, 2.5186, 2.4129, 2.3187])
    end

    GC.gc()

    lambda = 1.5
    dx = dy = 0.01
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 3.0
    nmodes = 6
    boundary = "0000"
    field = "scalar"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.7271, 2.5099, 2.1346, 2.0494, 1.8659, 1.7609])
    end

    GC.gc()

    lambda = 1.5
    dx = 0.005
    dy = 0.01
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 3.0
    nmodes = 6
    boundary = "0000"
    field = "scalar"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.40392, 2.16555, 1.74137, 1.37281, 1.30088, 0.99448])
    end

    GC.gc()

    lambda = 1.5
    dx = dy = 0.01
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 3.0
    nmodes = 6
    boundary = "0000"
    field = "ex"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.5941, 2.3611, 1.9313, 1.5619, 1.3834, 1.3105])
    end

    GC.gc()

    lambda = 1.5
    dx = dy = 0.01
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 3.0
    nmodes = 6
    boundary = "0000"
    field = "ey"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.7052, 2.4162, 2.0465, 1.9214, 1.8358, 1.7369])
    end

    GC.gc()

    lambda = 2.0
    dx = dy = 0.01
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 3.0
    nmodes = 6
    boundary = "SSSS"
    field = "ey"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.5286, 2.0119, 1.7045, 1.6636, 1.5389, 1.3714])
    end

    GC.gc()

    lambda = 2.0
    dx = dy = 0.01
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 3.0
    nmodes = 6
    boundary = "SSSS"
    field = "ey"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.5286, 2.0119, 1.7045, 1.6636, 1.5389, 1.3714])
    end

    GC.gc()

    lambda = 2.0
    dx = dy = 0.01
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 3.0
    nmodes = 4
    boundary = "0000"
    field = "scalar"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.5769, 2.2140, 1.6822, 1.5005])
    end

    GC.gc()

    lambda = 2.0
    dx = dy = 0.01
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 2
    nmodes = 4
    boundary = "0000"
    field = "scalar"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.2140, 1.6822, 1.5005, 1.4913])
    end

    GC.gc()

    lambda = 2.0
    dx = ones(200) ./ 100
    dx[1:5:200] .*= 2
    dy = ones(300) ./ 100
    dy[1:10:300] .*= 1.5
    n = ones(200, 300)
    n[1:99, 1:300] .= 1.5
    n[100:110, 1:300] .= 3
    n[100:150, 100:200] .= 3
    eps = n .^ 2
    guess = 3
    nmodes = 4
    boundary = "0000"
    field = "scalar"
    @test begin
        phi, neff = WGMODES.svmodes(lambda, guess, nmodes, dx, dy, eps, boundary, field)
        approxeq(neff, [2.6583, 2.3304, 1.8943, 1.7664])
    end
end
